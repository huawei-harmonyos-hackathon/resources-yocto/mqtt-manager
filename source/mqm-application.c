/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 * @file mqm-application.c
 */

#include "mqm-application.h"

#include <errno.h>
#include <fcntl.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

MqmApplication *mqm_application_new(const gchar *config, GError **error)
{
    MqmApplication *app = g_new0(MqmApplication, 1);

    g_assert(app);
    g_assert(error);

    g_ref_count_init(&app->rc);
#ifdef WITH_SYSTEMD
    /* construct sdnotify */
    app->sdnotify = mqm_sdnotify_new();
#endif
    /* construct options */
    app->options = mqm_options_new(config);
    /* construct options */
    app->executor = mqm_executor_new(app->options);
    /* construct dbus manager */
    app->dbusown = mqm_dbusown_new(app->options, app->executor);
    mqm_executor_set_dbusown(app->executor, app->dbusown);

    /* construct profile */
    app->adapter = mqm_adapter_new(app->options, app->executor);
    mqm_executor_set_mqtt_adapter(app->executor, app->adapter);

    /* construct main loop */
    app->mainloop = g_main_loop_new(NULL, TRUE);

    return app;
}

MqmApplication *mqm_application_ref(MqmApplication *app)
{
    g_assert(app);
    g_ref_count_inc(&app->rc);
    return app;
}

void mqm_application_unref(MqmApplication *app)
{
    g_assert(app);

    if (g_ref_count_dec(&app->rc) == TRUE) {
#ifdef WITH_SYSTEMD
        if (app->sdnotify != NULL)
            mqm_sdnotify_unref(app->sdnotify);
#endif
        if (app->dbusown != NULL)
            mqm_dbusown_unref(app->dbusown);

        if (app->options != NULL)
            mqm_options_unref(app->options);

        if (app->executor != NULL)
            mqm_executor_unref(app->executor);

        if (app->adapter != NULL)
            mqm_adapter_unref(app->adapter);

        if (app->mainloop != NULL)
            g_main_loop_unref(app->mainloop);

        g_free(app);
    }
}

GMainLoop *mqm_application_get_mainloop(MqmApplication *app)
{
    g_assert(app);
    return app->mainloop;
}

MqmStatus mqm_application_execute(MqmApplication *app)
{

    /* Start MQTT library */
    mqm_adapter_mqtt_start(app->adapter);

    /* run the main event loop */
    g_main_loop_run(app->mainloop);

    return MQM_STATUS_OK;
}
