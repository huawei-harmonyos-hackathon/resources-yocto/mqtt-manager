if exists('loaded_project_specific')
	finish
endif
let loaded_project_specific = 1

setlocal cindent
setlocal expandtab
setlocal shiftwidth=4
setlocal tabstop=4
setlocal softtabstop=4
setlocal textwidth=100
setlocal fo-=ro fo+=cql
